const fs = require('fs')

function createRandomFiles(callback) {

    try {

        fs.mkdir('randomJSONFiles', { recursive: true }, (err) => {
            if (err) {
                throw (err)
            }
        })

        console.log("directory created ! ")

        for (let index = 0; index < 10; index++) {

            fs.writeFile(`./randomJSONFiles/file${index}.json`, JSON.stringify(`this is file ${index}`), { encoding: 'utf-8' }, (err) => {
                if (err) {
                    throw (err)
                }
            })
            console.log(`file ${index} created`)
        }

        callback();

    }
    catch (err) {
        console.log(err)
    }
}

function deleteRandomFiles() {

    for (let index = 0; index < 10; index++) {

        fs.unlink(`./randomJSONFiles/file${index}.json`, (err) => {
            if (err) {
                throw (err.message)
            }
        })

        console.log("file successfully deleted ")
    }

}

module.exports = { createRandomFiles, deleteRandomFiles }

