const createRandomFiles = require('./../problem1.cjs').createRandomFiles
const deleteRandomFiles = require('./../problem1.cjs').deleteRandomFiles


createRandomFiles(() => {
    deleteRandomFiles()
})