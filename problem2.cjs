const fs = require('fs')

function readingAndWritingFile() {
    try {

        // point 1
        fs.readFile('./lipsum_1.txt', { encoding: 'utf8' }, (err, data) => {
            if (err) {
                console.log(err)
                return
            }

            //point 2
            const filterData = data.toUpperCase()
            const writeFilePath2 = './upperCaseData.txt'

            writeFileFunction(writeFilePath2, filterData, () => {

                const fileStorePath2 = './filenames.txt'
                const fileNameStore = 'upperCaseData.txt'
                appendFileFunction(fileStorePath2, fileNameStore, () => {

                    //point 3
                    const readFilePath3 = './upperCaseData.txt'

                    readFileFunction(readFilePath3, (data1) => {

                        const lowerCaseData = data1.toLowerCase()
                        const splitData = lowerCaseData.split(".")
                        const filePath3 = './lowerCaseFile.txt'

                        writeFileFunction(filePath3, splitData, () => {

                            const filePathForAppend = './filenames.txt'
                            const data = 'lowerCaseFile.txt'

                            appendFileFunction(filePathForAppend, data, () => {

                                // point 4
                                const readFilePath4 = './lowerCaseFile.txt'

                                readFileFunction(readFilePath4, (file4Data) => {
                                    const sortedData = file4Data.sort()

                                    const writeFilePath4 = './sortedData.txt'
                                    writeFileFunction(writeFilePath4, sortedData, () => {

                                        const filePathForAppend = './filenames.txt'
                                        const data = 'sortedData.txt'

                                        appendFileFunction(filePathForAppend, data, () => {

                                            // point 5
                                            const readFilePath5 = './filenames.txt'
                                            readFileContent(readFilePath5, (data) => {

                                                deleteFileFunction(data)
                                            })


                                        })
                                    })
                                })
                            })
                        })
                    })
                })
            })
        })

    }
    catch (err) {

        console.log(err)
    }
}

function deleteFileFunction(data) {

    data = data.split("\n")
    let size = data.length - 1
    for (let index = 0; index < size; index++) {
        fs.unlink('./' + data[index], (err) => {
            if (err) {
                throw (err)
                return
            }
        })
        console.log("file successfully deleted ")
    }
}
function readFileContent(filePath, callback) {
    fs.readFile(filePath, { encoding: 'utf8' }, (err, data) => {
        if (err) {
            throw (`error in file while reading file ${filePath} , ${err}`)
            return
        }
        console.log(`file ${filePath} successfully read `)

        callback(data)

    })
}

function readFileFunction(filePath, callback) {
    fs.readFile(filePath, { encoding: 'utf8' }, (err, data) => {
        if (err) {
            throw (`error in file while reading file ${filePath} , ${err}`)
            return
        }
        console.log(`file ${filePath} successfully read `)
        callback(JSON.parse(data))

    })
}

function writeFileFunction(filePath, data, callback) {
    fs.writeFile(filePath, JSON.stringify(data), { encoding: 'utf8' }, (err) => {
        if (err) {
            throw (`error in writing file and file path is ${filePath}`)
            return
        }
        console.log(`file ${filePath} successfully written `)

        callback()
    })
}

function appendFileFunction(filePath, data, callback) {
    fs.appendFile(filePath, data + "\n", { encoding: 'utf8' }, (err) => {
        if (err) {
            throw ("error in append file name for Lower Case File ")
            return
        }
        console.log(`file name successfully appended for ${filePath} FIle`)
        callback()
    })
}



module.exports = readingAndWritingFile